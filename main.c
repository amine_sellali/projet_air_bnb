#include "projet.h"
#define NB_ARRONDISSEMENTS 5
#define NB_QUARTIERS 60
#define MAX_LIGNE 200
#define MAX 100

int main(int argc, char* argv[]) 
{
    int mode = 0;  //0 = mode interactif, 1 = mode batch
    FILE* fichier_requetes = NULL;
    char requete[MAX];
    char nom_fichier[MAX_LIGNE];

    if(strcmp(argv[1],"-b") == 0)
    {
        mode = 1;                               //moode batch
        fichier_requetes = fopen(argv[3],"r");
        strcpy(nom_fichier,argv[2]);
        struct ordonnanceur ord;
        Liste res;
        res=NULL;
        init_ordonnanceur(&ord);
        charger_fichier(nom_fichier,&ord);  //on stocke les logements dans l'ordonnanceur
            
        while (fgets(requete,MAX,fichier_requetes) != NULL) //tant qu'on est pas à la fin du fichier
        {
            printf("%s\n",requete);
            lire2(requete,MAX);
            analyse_requete(requete,ord,&res,mode);
            desalloue_liste(&res);

        }
        return 0;
    }
        
    strcpy(nom_fichier,argv[1]);                //mode interactif
    struct ordonnanceur ord; 
    Liste res;
    res=NULL;
    
    init_ordonnanceur(&ord);
    charger_fichier(nom_fichier,&ord);
    
    while(strcmp(requete,"stop") != 0)
    {
        printf("Entrez votre requête : ");
        lire(requete,MAX);
        printf("\n");
        if (strcmp(requete,"stop") == 0)
        {
            break;
        }
        analyse_requete(requete,ord,&res,mode);
        desalloue_liste(&res);
    }
    return 0;
}
