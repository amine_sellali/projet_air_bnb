CC=clang
CFLAGS=-Wall -g 

prog: projet.o main.o
	$(CC) -o $@ $^ 

projet.o: projet.c
	$(CC) -o $@ -c $^ $(CFLAGS)

main.o: main.c
	$(CC) -o $@ -c $^ $(CFLAGS)

clean:
	rm -rf *.o
