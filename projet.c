#include "projet.h"
#define NB_ARRONDISSEMENTS 5
#define NB_QUARTIERS 60
#define MAX_LIGNE 200

//ajout en tete d'une liste chaînee
void ajout_tete(Logement log, Liste* pl) 
{
    Liste tmp = malloc(sizeof(Cellule));
    
    tmp->val = log;
    tmp->suiv = *pl;
    *pl = tmp;
    return;
}

//supprime la tete d'une liste chainée
void supprime_tete (Liste *pL)  //suppression du premier élément d'une liste cahînee
{
    Liste p_tmp = (*pL)->suiv;
    free(*pL);
    *pL = p_tmp;
}

//désaloue la liste
void desalloue_liste (Liste *pL) //vide une liste chaînee
{
    while (NULL != *pL){
        supprime_tete(pL);
    }
}

//désaloue l'ordonnanceur
void desalloue_ordonnanceur(struct ordonnanceur* ord) //vide un ordonnanceur
{
    for (int i=0; i<NB_ARRONDISSEMENTS; i++)
        desalloue_liste(&(ord->tab_arrondissements[i]));
}

//convertit une ligne d'un fichier en (struct) logement
Logement analyse_fichier(char* str) 
{
    char* tmp;
    Logement log;
    char * search = ",";
    
    tmp = strtok(str,search);
    log.id = atoi(tmp);
    
    tmp = strtok(NULL,",");
    strcpy(log.name,tmp);
    
    tmp = strtok(NULL,",");
    log.host_id = atoi(tmp);
    
    tmp = strtok(NULL,",");
    strcpy(log.host_name,tmp);
    
    tmp = strtok(NULL,",");
    strcpy(log.neighbourhood_group,tmp);
    
    tmp = strtok(NULL,",");
    strcpy(log.neighbourhood,tmp);
    
    tmp = strtok(NULL,",");
    log.latitude = strtof(tmp,NULL);
    
    tmp = strtok(NULL,",");
    log.longitude = strtof(tmp,NULL);
    
    tmp = strtok(NULL,",");
    strcpy(log.room_type,tmp);
    
    tmp = strtok(NULL,",");
    log.price = atoi(tmp);
    
    tmp = strtok(NULL,",");
    log.minimum_nights = atoi(tmp);
    
    tmp = strtok(NULL,",");
    log.number_of_reviews = atoi(tmp);
    
    tmp = strtok(NULL,",");
    strcpy(log.last_review,tmp);
    
    tmp = strtok(NULL,",");
    log.reviews_per_month = strtof(tmp,NULL);
    
    tmp = strtok(NULL,",");
    log.calculated_host_listings_count = atoi(tmp);
    
    tmp = strtok(NULL,",");
    log.availability_365 = atoi(tmp);
        
    return log;
}

//ajoute un (struct) logement à un ordonnanceur
void ajout_logement(Logement log, struct ordonnanceur* ord) 
{
    if(strcmp(log.neighbourhood_group,"Bronx") == 0) ajout_tete(log,&(ord->tab_arrondissements[0]));  //strcmp renvoi 0 si identique
    if(strcmp(log.neighbourhood_group,"Brooklyn") == 0) ajout_tete(log,&(ord->tab_arrondissements[1])); 
    if(strcmp(log.neighbourhood_group,"Manhattan") == 0) ajout_tete(log,&(ord->tab_arrondissements[2])); 
    if(strcmp(log.neighbourhood_group,"Queens") == 0) ajout_tete(log,&(ord->tab_arrondissements[3])); 
    if(strcmp(log.neighbourhood_group,"Staten Island") == 0) ajout_tete(log,&(ord->tab_arrondissements[4])); 
    
    return;
}

//stock les données d'un fichier dans un ordonnanceur
void charger_fichier(char* nom_fichier,struct ordonnanceur* p_ord) 
{
    Logement log;
    char *line_buf = NULL;
    size_t line_buf_size = 0;
    int line_count = 0;
    ssize_t line_size;
    FILE *fp = fopen(nom_fichier, "r");
    if (!fp)
    {   
        fprintf(stderr, "Error opening file '%s'\n", nom_fichier);
        return;
    }
    line_size = getline(&line_buf, &line_buf_size, fp);    
    while (line_size != -1)
    {
        line_count++;
        log=analyse_fichier(line_buf);
        ajout_logement(log,p_ord);
        line_size = getline(&line_buf, &line_buf_size, fp);
    }
    free(line_buf);
    line_buf = NULL;
    fclose(fp);    
}

//affiche les informations d'un logement
void affiche_logement(Logement log,int mode) 
{
    if(mode == 1)                  //mode batch
    {
        printf("%d,",log.id);    
        printf("%d,",log.host_id);
        printf("%s,",log.neighbourhood_group);
        printf("%s,",log.neighbourhood);
        printf("%d,",log.price);
        printf("%d,",log.minimum_nights);
        printf("%d,",log.number_of_reviews);
        printf("%d,",log.calculated_host_listings_count);
        printf("%d\n",log.availability_365);
    }
    
    if(mode == 0)                   //mode interactif
    {
        printf("%d\n",log.id);    
        printf("%s\n",log.name);
        printf("%d\n",log.host_id);
        printf("%s\n",log.host_name);
        printf("%s\n",log.neighbourhood_group);
        printf("%s\n",log.neighbourhood);
        printf("%f\n",log.latitude);
        printf("%f\n",log.longitude);
        printf("%s\n",log.room_type);
        printf("%d\n",log.price);
        printf("%d\n",log.minimum_nights);
        printf("%d\n",log.number_of_reviews);
        printf("%s\n",log.last_review);
        printf("%f\n",log.reviews_per_month);
        printf("%d\n",log.calculated_host_listings_count);
        printf("%d\n",log.availability_365);
    }
}

//affiche le contenu d'une liste chaînee
void affiche_liste(Liste l,int mode) 
{
    if (l==NULL) return;
    if(mode == 1)
    {
        affiche_logement(l->val,mode);
        affiche_liste(l->suiv,mode);
        return;
    }
    printf("----------------------------------\n \n");
    affiche_logement(l->val,mode);
    affiche_liste(l->suiv,mode);
}
    
//affiche le contenu d'un ordonnanceur
void affiche_ordonnanceur(struct ordonnanceur ord,int mode) 
{
    for (int i=0;i<NB_ARRONDISSEMENTS;i++)
    {
        if (ord.tab_arrondissements[i]!=NULL)
        {
            if (i==0) printf("Arrondissement: Bronx\n");
            if (i==1) printf("Arrondissement: Brooklyn\n");
            if (i==2) printf("Arrondissement: Manhattan\n");
            if (i==3) printf("Arrondissement: Queens\n");
            if (i==4) printf("Arrondissement: Staten Island \n");
            affiche_liste(ord.tab_arrondissements[i],mode);
        }
        printf("\n \n");
    }
}

//affiche la case n°i d'un ordonnanceur
void affiche_ordonnanceur_index(struct ordonnanceur ord, int i,int mode)
{
    if (ord.tab_arrondissements[i]!=NULL)
    {
        if(mode ==0)
        {
            if (i==0) printf("Arrondissement: Bronx\n");
            if (i==1) printf("Arrondissement: Brooklyn\n");
            if (i==2) printf("Arrondissement: Manhattan\n");
            if (i==3) printf("Arrondissement: Queens\n");
            if (i==4) printf("Arrondissement: Staten Island \n");
        }
        affiche_liste(ord.tab_arrondissements[i],mode);
    }
    printf("\n \n");
}


//initalise un ordonnanceur
void init_ordonnanceur(struct ordonnanceur* ord) 
{
    for (int i=0;i<NB_ARRONDISSEMENTS;i++)
        ord->tab_arrondissements[i]=NULL;
}


//fonction principale
void analyse_requete(char requete[],struct ordonnanceur ord, Liste* res,int mode) //traite une requête
{
    char* zone = NULL;
    char* operation = NULL;
    char* seuil = NULL;
    int SEUIL;
    int arrondissement;
    int taille_requete=compteur_mots(requete);
    int nb_requete=(taille_requete-1)/2;
    char* tab[13];
    Liste res_tmp;
    res_tmp=NULL;

    
    if(requete[0] == '"') //cas où zone est un mot composé
    {
        zone = strtok(requete,"\"");
    }
    else
    {
        zone = strtok(requete," ");
    }
    
    operation = strtok(NULL," ");
    
    
    seuil = strtok(NULL," ");
    
    tab[0]=zone; 
    tab[1]=operation;
    tab[2]=seuil;
    //on crée un tableau dans lequel on stocke successivement zone, operation et seuil
    for (int i=3; i<taille_requete; i++)
    {
        tab[i]=strtok(NULL," ");
    }
    
    
    if (verif_arrondissement(zone)==1) //zone est un arrondissement
    {
        if (strcmp(requete,"Bronx") == 0) {
            arrondissement=0;
            if (operation==NULL) { //pas d'operation entrée par l'utilisateur
                affiche_ordonnanceur_index(ord,0,mode);
                return;
            }
        }
        if (strcmp(requete,"Brooklyn") == 0) {
            arrondissement=1;
            if (operation==NULL) {
                affiche_ordonnanceur_index(ord,1,mode);
                return;
            }
        }
        if (strcmp(requete,"Manhattan") == 0) {
            arrondissement=2;
            if (operation==NULL) {
                affiche_ordonnanceur_index(ord,2,mode);
                return;
            }
        }
        if (strcmp(requete,"Queens") == 0) {
            arrondissement=3;
            if (operation==NULL) {
                affiche_ordonnanceur_index(ord,3,mode);
                return;
            }
        }
        if (strcmp(zone,"Staten Island") == 0) {
            arrondissement=4;
            if (operation==NULL) {
                affiche_ordonnanceur_index(ord,4,mode);
                return;
            }
        }    
        if(strcmp(requete,"global") == 0 && operation==NULL)
        {
            affiche_ordonnanceur(ord,mode);
            return;
        }
            
        for (int i=1; i<nb_requete+2; i=i+2) //traite le cas où l'utilisateur a entré plusieurs opérations
        {
            operation=tab[i];
            seuil=tab[i+1];

            if (seuil != NULL)
            {
                SEUIL = atoi(seuil);
            }
            
            if (i==1)
            {
                operations(operation,SEUIL,zone,ord,arrondissement,res);
            }
            
            if (i>1)
            {
                operations(operation,SEUIL,zone,ord,arrondissement,&res_tmp);
                *res=intersection_ensemble(*res,res_tmp);
                res_tmp=NULL;
            }
        }
    }
    //zone est donc un nom de quartier
    if (verif_arrondissement(zone)==0)
    {
        arrondissement=index_arrondissement(zone, ord);
        if (operation==NULL) 
        {
            affiche_ordonnanceur_index_et_quartier(ord, arrondissement, zone,mode);
        return;
        }

        for (int i=1; i<nb_requete+2; i=i+2)
        {
            operation=tab[i];
            seuil=tab[i+1];

            if (seuil != NULL)
            {
                SEUIL = atoi(seuil);

            }
            
            if (i==1)
            {
                operations_quartiers(operation,SEUIL,zone,ord,arrondissement, zone, res);
            }
            
            if (i>1)
            {
                operations_quartiers(operation,SEUIL,zone,ord,arrondissement, zone, &res_tmp);
                *res=intersection_ensemble(*res,res_tmp);
                res_tmp=NULL;
            }
        }        
    }
    affiche_liste(*res,mode);
    return;
}

int lire(char *chaine, int longueur) //ajoute un '\0' à la dans la chaine de caractere à la place du retour à la ligne
{
    char *positionEntree = NULL;
 
    // On lit le texte saisi au clavier
    if (fgets(chaine, longueur, stdin) != NULL)
    {
        positionEntree = strchr(chaine, '\n'); // On recherche le retour à la ligne
        if (positionEntree != NULL) 
        {
            *positionEntree = '\0'; // On remplace ce caractère par '\0'
        }
        return 1;
    }
    else
    {
        return 0; 
    }
}

int lire2(char *chaine, int longueur)
{
    char *positionEntree = NULL;
    positionEntree = strchr(chaine, '\n');
    if (positionEntree != NULL) 
    {
        *positionEntree = '\0'; 
        return 1;
    }
    else
    {
        return 0; 
    }
}
    


//recherche quelle opértation effectuer
void operations(char operation[],int SEUIL,char requete[],struct ordonnanceur ord,int arrondissement, Liste* res)
{
    if (strcmp(operation,"price_max") == 0) 
    {
        if (strcmp(requete,"global") != 0)
            price_max(SEUIL,ord.tab_arrondissements[arrondissement],res);
        else
        {
            for (int i=0;i<NB_ARRONDISSEMENTS;i++) {
                price_max(SEUIL,ord.tab_arrondissements[i], res);
            }
        }
    }

    if (strcmp(operation,"price_min") == 0)
    {
        if (strcmp(requete,"global") != 0)
            price_min(SEUIL,ord.tab_arrondissements[arrondissement],res);
        else
        {
            for (int i=0;i<NB_ARRONDISSEMENTS;i++) {
                price_min(SEUIL,ord.tab_arrondissements[i],res);
            }
        }
    }
    
    if (strcmp(operation,"reviews_min") == 0)
    {
        if (strcmp(requete,"global") != 0)
            reviews_min(SEUIL,ord.tab_arrondissements[arrondissement], res);
        else
        {
            for (int i=0;i<NB_ARRONDISSEMENTS;i++) {
                reviews_min(SEUIL,ord.tab_arrondissements[i], res);
            }
        }
    }
    
    if (strcmp(operation,"nights_min") == 0)
    {
        if (strcmp(requete,"global") != 0)
            nights_min(SEUIL,ord.tab_arrondissements[arrondissement],res);
        else
        {
            for (int i=0;i<NB_ARRONDISSEMENTS;i++) {
                nights_min(SEUIL,ord.tab_arrondissements[i],res);
            }
        }
    }
    
    if (strcmp(operation,"availability_min") == 0)
    {
        if (strcmp(requete,"global") != 0)
            availability_min(SEUIL,ord.tab_arrondissements[arrondissement],res);
        else
        {
            for (int i=0;i<NB_ARRONDISSEMENTS;i++) {
                availability_min(SEUIL,ord.tab_arrondissements[i],res);
            }
        }
    }
    
    if (strcmp(operation,"count_min") == 0)
    {
        if (strcmp(requete,"global") != 0)
            count_min(SEUIL,ord.tab_arrondissements[arrondissement],res);
        else
        {
            for (int i=0;i<NB_ARRONDISSEMENTS;i++) {
                count_min(SEUIL,ord.tab_arrondissements[i],res);
            }
        }
    }
    return;
}

void operations_quartiers(char operation[],int SEUIL,char requete[],struct ordonnanceur ord,int arrondissement, char* quartier, Liste* res) //recherche de quelle operation effectuer (quartier)
{
    if (strcmp(operation,"price_max") == 0) 
    {
        price_max_quartier(SEUIL,ord.tab_arrondissements[arrondissement], quartier,res);
    }
    
    if (strcmp(operation,"price_min") == 0)
    {
        price_min_quartier(SEUIL,ord.tab_arrondissements[arrondissement], quartier,res);
    }
    
    if (strcmp(operation,"reviews_min") == 0)
    {
        reviews_min_quartier(SEUIL,ord.tab_arrondissements[arrondissement], quartier,res);
    }
    
    if (strcmp(operation,"nights_min") == 0)
    {
        nights_min_quartier(SEUIL,ord.tab_arrondissements[arrondissement], quartier,res);
    }
    
    if (strcmp(operation,"availability_min") == 0)
    {
        availability_min_quartier(SEUIL,ord.tab_arrondissements[arrondissement], quartier,res);
    }
    
    if (strcmp(operation,"count_min") == 0)
    {
        count_min_quartier(SEUIL,ord.tab_arrondissements[arrondissement], quartier,res);
    }
    return;
}

    
//fonction qui stocke dans la liste res tous les logements de la liste "Liste" dont le prix est inférieur ou égal au prix max
void price_max(int max, Liste liste, Liste* res) 
{
    int price;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        price=log.price;
        if (price<=max)
        {
            ajout_tete(log,res);
            
        }
        liste=liste->suiv;
    }
}

//meme fonction que price_max mais pour un quartier spécifié
void price_max_quartier(int max, Liste liste, char* quartier, Liste* res) 
{
    int price;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        price=log.price;
        if (price<=max && strcmp(quartier,log.neighbourhood)==0)
        {
            ajout_tete(log,res);
        }
        liste=liste->suiv;
    }
}

void price_min(int min, Liste liste, Liste* res) 
{
    int price;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        price=log.price;
        if (price>=min)
        {
            ajout_tete(log,res);
        }
        liste=liste->suiv;
    }
}

void price_min_quartier(int min, Liste liste, char* quartier, Liste* res) 
{
    int price;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        price=log.price;
        if (price>=min && strcmp(quartier,log.neighbourhood)==0)
        {
            ajout_tete(log,res);
        }
        liste=liste->suiv;
    }
}

void reviews_min(int min, Liste liste, Liste* res) 
{
    int reviews;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        reviews=log.number_of_reviews;
        if (reviews>=min)
        {
            ajout_tete(log,res);
        }
        liste=liste->suiv;
    }
}

void reviews_min_quartier(int min, Liste liste, char* quartier, Liste* res)
{
    int reviews;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        reviews=log.number_of_reviews;
        if (reviews>=min && strcmp(quartier,log.neighbourhood)==0)
        {
            ajout_tete(log,res);
        }
        liste=liste->suiv;
    }
}

void nights_min(int min, Liste liste, Liste* res) 
{
    int nights;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        nights=log.minimum_nights;
        if (nights>=min)
        {
            ajout_tete(log,res);
        }
        liste=liste->suiv;
    }
}
    
void nights_min_quartier(int min, Liste liste, char* quartier, Liste* res)
{
    int nights;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        nights=log.minimum_nights;
        if (nights>=min && strcmp(quartier,log.neighbourhood)==0)
        {
            ajout_tete(log,res);
        }
        liste=liste->suiv;
    }
}
    
    
void availability_min(int min, Liste liste, Liste* res) 
{
    int availability;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        availability=log.availability_365;
        if (availability>=min)
        {
            ajout_tete(log,res);
        }
        liste=liste->suiv;
    }
}
        
void availability_min_quartier(int min, Liste liste, char* quartier, Liste* res)
{
    int availability;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        availability=log.availability_365;
        if (availability>=min && strcmp(quartier,log.neighbourhood)==0)
        {
            ajout_tete(log,res);
        }
        liste=liste->suiv;
    }
}
        
        
void count_min(int min, Liste liste, Liste* res) 
{
    int count;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        count=log.calculated_host_listings_count;
        if (count>=min)
        {
            ajout_tete(log,res);
        }
        liste=liste->suiv;
    }
}

void count_min_quartier(int min, Liste liste, char* quartier, Liste* res) 
{
    int count;
    Logement log;
    while (liste != NULL)
    {
        log=liste->val;
        count=log.calculated_host_listings_count;
        if (count>=min && strcmp(quartier,log.neighbourhood)==0)
        {
            ajout_tete(log,res);
        }
        liste=liste->suiv;
    }
}

//initialise la structure nom_arrondissements contenant les noms des arrondissements
void init_nom_arrondissements(struct nom_arrondissements* tab)
{
    tab->arrondissements[0]="Bronx";
    tab->arrondissements[1]="Brooklyn";
    tab->arrondissements[2]="Manhattan";
    tab->arrondissements[3]="Queens";
    tab->arrondissements[4]="Staten Island";
}

//fonction renvoyant l'indice de l'arrondissement auquel le quartier appartient
int index_arrondissement(char* quartier, struct ordonnanceur ord)
{
    int index;
    for (int i=0; i<NB_ARRONDISSEMENTS;i++)
    {
        Liste liste=ord.tab_arrondissements[i];
        while (liste!=NULL)
        {
            if (strcmp(quartier,liste->val.neighbourhood)==0)
            {
                index=i;
            }
            liste=liste->suiv;
        }
    }
    return index;
}
 
//affiche les logements de l'arrondissement i appartenant au quartier "quartier"
void affiche_ordonnanceur_index_et_quartier(struct ordonnanceur ord, int i, char* quartier,int mode)
{
    if (ord.tab_arrondissements[i]!=NULL)
    {
        if(mode == 0)
        {
            if (i==0) printf("Arrondissement: Bronx\n");
            if (i==1) printf("Arrondissement: Brooklyn\n");
            if (i==2) printf("Arrondissement: Manhattan\n");
            if (i==3) printf("Arrondissement: Queens\n");
            if (i==4) printf("Arrondissement: Staten Island \n");
        }
        
        affiche_liste_quartier(ord.tab_arrondissements[i], quartier,mode);
    }
    printf("\n \n");
}

void affiche_liste_quartier(Liste l, char* quartier,int mode)   
{
    if (l==NULL) return;
    if (strcmp(quartier, l->val.neighbourhood)==0) 
    {
        if(mode == 0)
        {
            printf("----------------------------------\n \n");
        }
        affiche_logement(l->val,mode);
    }
    affiche_liste_quartier(l->suiv, quartier,mode);
}



//renvoi 1 si s est un arrondissement, 0 sinon
int verif_arrondissement(char* s)
{
    struct nom_arrondissements tab;
    init_nom_arrondissements(&tab);
    if (strcmp(s,"global")==0) return 1;
    for (int i=0;i<NB_ARRONDISSEMENTS;i++)
    {
        if (strcmp(s,tab.arrondissements[i])==0)
        {
            return 1;
        }
    }
    return 0;
} 

//fonction qui compte de nb de mot dans une requete
int compteur_mots(char* str)
{
    int i, count = 1;
    for(i = 0; str[i] != '\0'; i++)
    {
        if(str[i] == ' ' || str[i] == '\t' || str[i] == '\n')
        {   
            count++;  
        } 
    }  
  return count;
}
    
//fonction qui retourne l'intersection de deux listes
Liste intersection_ensemble (Liste l1, Liste l2)
{
    Liste l = NULL;
    Liste l_tmp;
    while (l1 != NULL){
        l_tmp = l2;
        while (l_tmp != NULL){
            if (l1->val.id == l_tmp->val.id){ 
                ajout_tete(l1->val,&l);
            }
            l_tmp = l_tmp->suiv;
        }
        l1 = l1->suiv;
    }
    return l;
}
