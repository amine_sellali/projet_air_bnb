#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define SIZE 100
#define NB_ARRONDISSEMENTS 5



typedef struct logement {
    int id;
    char name[SIZE];
    int host_id;
    char host_name[SIZE];
    char neighbourhood_group[SIZE]; //arrondissements
    char neighbourhood[SIZE]; //quartiers
    float latitude;
    float longitude;
    char room_type[SIZE];
    int price;
    int minimum_nights;
    int number_of_reviews;
    char last_review[SIZE];
    float reviews_per_month;
    int calculated_host_listings_count;
    int availability_365;
} Logement;

typedef struct cellule {
    struct logement val;
    struct cellule* suiv;
}Cellule;

typedef Cellule* Liste;


struct ordonnanceur
{
    Liste tab_arrondissements[NB_ARRONDISSEMENTS]; //case 0=Bronx, case 1= Brooklyn, case2=Manhattan, case3=Queens, case4=Staten Island 
};

struct nom_arrondissements {
    char* arrondissements[NB_ARRONDISSEMENTS];
};

void ajout_tete(Logement log, Liste* pl);

Logement analyse_fichier(char* str);

void charger_fichier(char* nom_fichier,struct ordonnanceur* ord);

void ajout_logement(Logement log, struct ordonnanceur* ord);

void affiche_logement(Logement log,int mode);

void affiche_liste(Liste l,int mode);

void affiche_ordonnanceur(struct ordonnanceur ord,int mode);

void init_ordonnanceur(struct ordonnanceur* ord);

void analyse_requete(char requete[],struct ordonnanceur ord, Liste* res,int mode);

int lire(char *chaine, int longueur); //lit la requête et la stocke dans chaine

void operations(char operation[],int SEUIL,char requete[],struct ordonnanceur ord,int arrondissement, Liste* res);

void operations_quartiers(char operation[],int SEUIL,char requete[],struct ordonnanceur ord,int arrondissement, char* quartier, Liste* res);

void price_max(int max, Liste liste, Liste* res);

void price_max_quartier(int max, Liste liste, char* quartier, Liste* res);

void price_min(int min, Liste liste, Liste* res);

void price_min_quartier(int min, Liste liste, char* quartier, Liste* res);

void reviews_min(int min, Liste liste, Liste* res);

void reviews_min_quartier(int min, Liste liste, char* quartier, Liste* res);

void nights_min(int min, Liste liste, Liste* res);

void nights_min_quartier(int min, Liste liste, char* quartier, Liste* res);

void availability_min(int min, Liste liste, Liste* res);

void availability_min_quartier(int min, Liste liste, char* quartier, Liste* res);

void count_min(int min, Liste liste, Liste* res);

void count_min_quartier(int min, Liste liste, char* quartier, Liste* res);

int verif_arrondissement(char* );

int index_arrondissement(char* quartier, struct ordonnanceur ord);

void affiche_ordonnanceur_index_et_quartier(struct ordonnanceur ord, int i, char* quartier,int mode);

void affiche_liste_quartier(Liste l, char* quartier,int mode);  

int compteur_mots(char* str);

void supprime_tete (Liste *pL);

void desalloue_liste (Liste *pL);

void desalloue_ordonnanceur(struct ordonnanceur* ord);

Liste intersection_ensemble (Liste l1, Liste l2);

int lire2(char *chaine, int longueur);
