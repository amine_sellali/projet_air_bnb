# Projet de Programmation Avancée · AirBnB New York

## Introduction

Notre programme possède 2 modes de fonctionnement. 
Premièrement, le mode interactif, dans lequel l'utilisateur spécifie
une à une les différentes requêtes.

Deuxièmement, le mode batch, dans lequel les requêtes lues par le programme 
sont cette fois, écrites dans un fichier. Le programme répond alors à toutes les requêtes.

Comme demandé dans le cahier des charges, les requêtes s'écriront de la manière suivante :


```bash
zone [operation seuil]*
```

`zone` peut être : `global`, un arrondissement de New York ou un quartier de New York.
En prenant soin de mettre entre guillemets les noms avec espace exemple : `"Staten Island"`.

`operation` peut être : `price_max`, `price_min`, `reviews_min`, `nights_min`, `availability_min`, `count_min`.

`seuil` peut prendre n'importe quelle valeur entière.


## Fonctionnement du Programme

### Compilation

Pour compiler le programme, il suffira de taper la commande `make` dans le terminal.
La compilation automatique, générera un exécutable nommé : `./prog`.

### Mode Interactif

Pour lancer le mode interactif il faudra spécifier quel fichier au format `CSV` va être exploité.
Exemple : `./prog airbnb_ny_2019.csv`

Si on veut tester le programme avec un échantillon on aura alors simplement :
`./prog echantillon.csv`

Le programme demandera alors à l'utilisateur de spécifier sa requête :
`Entrez votre requête :` 

Les résultats s'afficheront de la manière suivante :
```bash
Entrez votre requête : Midwood price_max 30

----------------------------------
 
24119328
Charming Brooklyn Gem with Beautiful Views
8612450
Kareem
Brooklyn
Midwood
40.613789
-73.968262
Private room
20
3
4
2018-05-31
0.260000
1
18
----------------------------------
 
33632933
Comfy and quiet bed for women travelers
224150
Svetlana
Brooklyn
Midwood
40.614891
-73.945412
Shared room
26
2
5
2019-05-10
1.830000
1
0
```

Pour arrêter l'exécution du programme il suffit de taper : `stop`.

### Mode batch

Pour exécuter le mode batch il faudra utiliser `-b` et spécifier le fichier `CSV` ainsi que
le fichier texte à partir duquel seront lues les requêtes.
Exemple : `./prog -b airbnb_ny_2019 requetes.txt`.
Le programmera s'exécutera et s'arrêtera une fois qu'il aura traité la dernière requête.

Les résultats s'afficheront de la manière suivante :
```bash
Gramercy nights_min 20

387735,1146958,Manhattan,Gramercy,125,30,128,4,0
1077176,5927702,Manhattan,Gramercy,131,30,23,2,33
1077179,5927702,Manhattan,Gramercy,250,30,17,2,327
1824808,1146958,Manhattan,Gramercy,195,30,104,4,333
2645558,2653156,Manhattan,Gramercy,200,30,38,1,170
3646551,2119276,Manhattan,Gramercy,150,30,2,39,331
3983643,1475015,Manhattan,Gramercy,130,30,2,52,335
4352537,22595345,Manhattan,Gramercy,64,26,47,1,331
4509560,2119276,Manhattan,Gramercy,140,30,9,39,344
4621217,2119276,Manhattan,Gramercy,140,30,7,39,332
4621713,2119276,Manhattan,Gramercy,185,30,5,39,1
4791389,4923436,Manhattan,Gramercy,250,30,2,1,0
5940920,30836013,Manhattan,Gramercy,209,30,27,1,197
6051782,2119276,Manhattan,Gramercy,150,30,10,39,67
7398163,28269324,Manhattan,Gramercy,45,30,2,1,0
7853561,2119276,Manhattan,Gramercy,200,30,5,39,331
8809245,3333577,Manhattan,Gramercy,410,30,3,2,346
9771893,16098958,Manhattan,Gramercy,150,30,1,96,282
9807758,16098958,Manhattan,Gramercy,165,30,2,96,321
10339835,3599608,Manhattan,Gramercy,200,28,4,1,73
11162564,2119276,Manhattan,Gramercy,130,30,6,39,246
13112855,16098958,Manhattan,Gramercy,180,30,1,96,281
14366544,16098958,Manhattan,Gramercy,179,30,2,96,345
16215826,105328014,Manhattan,Gramercy,90,30,5,1,154
17494289,118539035,Manhattan,Gramercy,99,120,2,1,167
21263008,153735730,Manhattan,Gramercy,766,30,13,2,352
22193650,83637,Manhattan,Gramercy,500,28,5,1,173
31682228,237569289,Manhattan,Gramercy,220,90,6,1,0
```

